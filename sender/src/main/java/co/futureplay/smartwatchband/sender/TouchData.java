package co.futureplay.smartwatchband.sender;

public class TouchData {
    private static final byte ACTION_DOWN = 0x00;
    private static final byte ACTION_UP = 0x04;
    private static final byte ACTION_MOVE = 0x07;
    private static final byte ACTION_NOT_TOUCHED = 0x01;

    private final byte id;
    private byte action = ACTION_NOT_TOUCHED;
    private int x;
    private int y;
    private int z;
    private boolean updated;
    private byte[] data = new byte[8];

    public TouchData(byte id) {
        this.id = id;
    }

    public void onTouchDown(int x, int y, int z) {
        this.action = ACTION_DOWN;
        this.x = x;
        this.y = y;
        this.z = z;
        updated = true;
    }

    public void onTouchUp(int x, int y, int z) {
        this.action = ACTION_UP;
        this.x = x;
        this.y = y;
        this.z = z;
        updated = true;
    }

    public void onTouchMove(int x, int y, int z) {
        if (!updated || action == ACTION_MOVE) {
            this.action = ACTION_MOVE;
            this.x = x;
            this.y = y;
            this.z = z;
            updated = true;
        }
    }

    public byte[] consume() {
        if (updated) {
            data[0] = action;
        } else {
            if (action == ACTION_DOWN || action == ACTION_MOVE) {
                data[0] = ACTION_MOVE;
            } else {
                data[0] = ACTION_NOT_TOUCHED;
            }
        }

        data[1] = id;
        data[2] = (byte) (x >>> 8);
        data[3] = (byte) x;
        data[4] = (byte) (y >>> 8);
        data[5] = (byte) y;
        data[6] = (byte) (z >>> 8);
        data[7] = (byte) z;

        updated = false;
        return data;
    }

    @Override
    public String toString() {
        return String.format("%02X %04X %04X %04X ", action, x, y, z) + (updated ? "Y": "N");
    }
}
