package co.futureplay.smartwatchband.sender;

import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

public class MultiTouchData {
    private static final int TOUCH_DATA_SIZE = 8;
    private final int targetWidth;
    private final int targetHeight;
    private final TouchData[] touches = new TouchData[5];
    private final byte[] sendBuffer = new byte[TOUCH_DATA_SIZE * touches.length];
    private int viewWidth;
    private int viewHeight;

    public MultiTouchData(final int targetWidth, final int targetHeight) {
        this.targetWidth = targetWidth;
        this.targetHeight = targetHeight;

        for (int i = 0; i < touches.length; i++) {
            touches[i] = new TouchData((byte) i);
        }
    }

    public void onTouchEvent(MotionEvent event) {
        int index = MotionEventCompat.getActionIndex(event);
        int action = MotionEventCompat.getActionMasked(event);

        if (action == MotionEvent.ACTION_MOVE) {
            for (int i = 0; i < event.getPointerCount(); i++) {
                int pointerId = event.getPointerId(i);
                touches[pointerId].onTouchMove(getTargetX(MotionEventCompat.getX(event, i)), getTargetY(MotionEventCompat.getY(event, i)), 1);
            }
        } else if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
            int pointerId = event.getPointerId(index);
            touches[pointerId].onTouchDown(getTargetX(MotionEventCompat.getX(event, index)), getTargetY(MotionEventCompat.getY(event, index)), 1);
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) {
            int pointerId = event.getPointerId(index);
            touches[pointerId].onTouchUp(getTargetX(MotionEventCompat.getX(event, index)), getTargetY(MotionEventCompat.getY(event, index)), 1);
        }
    }

    @Override
    public String toString() {
        String rv = "";
        for (TouchData touch : touches) {
            rv += touch.toString() + "\n";
        }
        return rv;
    }

    public byte[] getSendData() {
        for (int i = 0; i < touches.length; i++) {
            System.arraycopy(touches[i].consume(), 0, sendBuffer, i * TOUCH_DATA_SIZE, TOUCH_DATA_SIZE);
        }
        return sendBuffer;
    }

    public void setViewSize(int width, int height) {
        viewWidth = width;
        viewHeight = height;
        System.out.println("ViewSize : " + width + " X " + height);
    }

    private int getTargetX(float viewX) {
        if (viewWidth > 0) {
            return ((int) viewX) / (viewWidth / targetWidth);
        } else {
            return (int) viewX;
        }
    }

    private int getTargetY(float viewY) {
        if (viewHeight > 0) {
            return ((int) viewY) / (viewHeight / targetHeight);
        } else {
            return (int) viewY;
        }
    }
}
