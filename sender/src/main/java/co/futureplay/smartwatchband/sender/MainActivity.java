package co.futureplay.smartwatchband.sender;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.UUID;

import co.futureplay.android.commons.bluetooth.BluetoothActivity;


public class MainActivity extends BluetoothActivity implements BluetoothActivity.OnSelectListener {
    private static final UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final UUID RFCOMM_UUID = UUID.fromString("00000003-0000-1000-8000-00805F9B34FB");

    private Thread clientThread = null;
    private Thread serverThread = null;
    private InputStream clientInputStream;
    private OutputStream clientOutputStream;
    private InputStream serverInputStream;
    private OutputStream serverOutputStream;
    private byte[] touchData = new byte[128];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverThread = new ServerThread();
        serverThread.start();

        setOnSelectListener(this);
        searchDevices();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static String getByteHexString(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        int count = 0;

        for (byte b : bytes) {
            result.append(String.format("%02x ", b));

            ++count;

            if (count == 10) {
                result.append("          ");
            } else if (count == 20) {
                result.append("\n");
                count = 0;
            }
        }

        return result.toString();
    }

    @Override
    public void onSelect(BluetoothDevice bluetoothDevice) {
        try {
            BluetoothSocket bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(SERIAL_UUID);
            bluetoothSocket.connect();

            clientThread = new ClientThread(bluetoothSocket);
            clientThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send() throws IOException {
        if (serverOutputStream != null) {
            serverOutputStream.write(touchData);
        }
    }

    public void requestData() {
        if (clientOutputStream != null) {
            try {
                clientOutputStream.write(new byte[]{'T'});
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class ClientThread extends Thread {
        private BluetoothSocket socket;

        public ClientThread(BluetoothSocket socket) {
            try {
                this.socket = socket;
                clientInputStream = socket.getInputStream();
                clientOutputStream = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            byte[] buffer = new byte[16];
            int len;

            while (!Thread.interrupted() && socket.isConnected()) {
                try {
                    len = clientInputStream.read(buffer, 0, 16);

                    if (buffer[6] == buffer[7] && buffer[6] == (byte)0xff) {
                        touchData = new byte[16];
                        send();
                        continue;
                    }

                    if (clientInputStream.available() > 0) {
                        clientInputStream.skip(clientInputStream.available());
                    }

                    Log.d("FP", "R "+getByteHexString(Arrays.copyOf(buffer, len)));

                    if (len >= 16) {
                        len = 16;
                    } else if (len >= 8) {
                        len = 8;
                    } else {
                        continue;
                    }

                    Log.d("FP", "S "+getByteHexString(Arrays.copyOf(buffer, len)));

                    System.arraycopy(buffer, 0, touchData, 0, len);
                } catch (IOException e) {
                    Log.e(TAG, "IO Error", e);
                    break;
                }

                try {
                    send();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class ServerThread extends Thread {
        private BluetoothServerSocket serverSocket;

        @Override
        public void run() {
            listen();
        }

        private void listen() {
            try {
                serverSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("FPBTTerm", RFCOMM_UUID);
            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "Failed to listen", Toast.LENGTH_SHORT).show();
                finish();
            }

            while (!Thread.interrupted()) {
//                updateState("LISTENING ");

                if (serverOutputStream != null) {
                    continue;
                }

                BluetoothSocket clientSocket = null;
                try {
                    clientSocket = serverSocket.accept();
                } catch (IOException e) {
                    break;
                }

                try {
                    serverInputStream = clientSocket.getInputStream();
                    serverOutputStream = clientSocket.getOutputStream();

                    requestData();
                } catch (IOException e) {
                    if (serverInputStream != null) {
                        try {
                            serverInputStream.close();
                        } catch (IOException e2) {
                        }
                    }
                    if (serverInputStream != null) {
                        try {
                            serverOutputStream.close();
                        } catch (IOException e2) {
                        }
                    }
                    continue;
                }

                ioLoop();
            }
        }

        private void ioLoop() {
//            updateState("CONNECTED");
            Integer cmd = null;
            while (!Thread.interrupted()) {
                try {
                    if (cmd == null) {
                        cmd = serverInputStream.read();
                    } else {
                        if (cmd == 'T') {
                            requestData();
                        } else {
                            Log.w(TAG, "Unknown command " + cmd);
                        }
                        cmd = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "IO Error", e);
                    break;
                }
            }
        }
    }
}
