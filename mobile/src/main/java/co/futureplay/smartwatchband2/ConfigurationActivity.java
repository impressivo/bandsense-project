package co.futureplay.smartwatchband2;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Bapul on 15. 4. 14..
 */
public class ConfigurationActivity extends PreferenceActivity {
    public final static String KEY_RESOLUTION = "pref_resolution";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.configuration);
    }
}
