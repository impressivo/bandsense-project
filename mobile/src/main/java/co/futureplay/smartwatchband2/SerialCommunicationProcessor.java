package co.futureplay.smartwatchband2;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import co.futureplay.smartwatchband2.common.ContextObject;
import co.futureplay.smartwatchband2.common.FuturePlay;
import co.futureplay.smartwatchband2.common.ImpressivoDataProcessor;

public class SerialCommunicationProcessor extends ContextObject implements SerialInputOutputManager.Listener {
    private ExecutorService mExecutor;
    private SerialInputOutputManager serialManager;
    private UsbSerialDriver driver;
    private ImpressivoDataProcessor impressivoDataProcessor;

    private boolean connected;

    public SerialCommunicationProcessor(Context context, ImpressivoDataProcessor.Listener listener) {
        super(context);
        impressivoDataProcessor = new ImpressivoDataProcessor(listener);
    }

    public void start() {
        UsbManager manager = (UsbManager) getContext().getSystemService(Context.USB_SERVICE);
        driver = UsbSerialProber.acquire(manager);

        if (driver != null) {
            try {
                driver.open();
//                driver.setBaudRate(115200);
                driver.setBaudRate(9600);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                return;
            }

            serialManager = new SerialInputOutputManager(driver, this);

            mExecutor = Executors.newSingleThreadExecutor();
            mExecutor.submit(serialManager);

            connected = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!connected) {
                        request();

                        try {
                            Thread.sleep(300,0);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
        else {
            Log.d(FuturePlay.TAG, "no available device");
        }
    }

    public void request() {
        if (serialManager == null) {
            Log.d(FuturePlay.TAG, "serialManager null!");
            return;
        }

        byte[] data = {'F'};
        serialManager.writeAsync(data);
    }

    @Override
    public void onNewData(byte[] data) {
        connected = true;
        impressivoDataProcessor.onNewData(data);
        request();
    }

    @Override
    public void onRunError(Exception e) {
        e.printStackTrace();
    }
}
