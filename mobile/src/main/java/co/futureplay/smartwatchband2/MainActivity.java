package co.futureplay.smartwatchband2;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.wearable.Node;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import co.futureplay.android.commons.bluetooth.BluetoothActivity;
import co.futureplay.smartwatchband2.common.BandTouchEvent;
import co.futureplay.smartwatchband2.common.Bluetooth.EXAsyncInputStreamReader;
import co.futureplay.smartwatchband2.common.Bluetooth.EXAsyncOutputStreamWriter;
import co.futureplay.smartwatchband2.common.EXDisplayUtil;
import co.futureplay.smartwatchband2.common.FuturePlay;
import co.futureplay.smartwatchband2.common.ImpressivoDataProcessor;
import co.futureplay.smartwatchband2.common.WearableMessageClient;
import co.futureplay.smartwatchband2.visualization.BandTouchRenderer;
import co.futureplay.smartwatchband2.visualization.BandTouchSurfaceView;
import co.futureplay.smartwatchband2.visualization.VizPoint;


public class MainActivity extends BluetoothActivity implements ImpressivoDataProcessor.Listener, BluetoothActivity.OnSelectListener, EXAsyncInputStreamReader.Listener, EXAsyncOutputStreamWriter.Listener, BandTouchRenderer.Listener {
    private static final UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private WearableMessageClient messageClient;

//    private SerialCommunicationProcessor serialCommunicationProcessor;

    private BandTouchSurfaceView bandTouchSurfaceView;
    private BandTouchRenderer bandTouchRenderer;
    private BluetoothSocket bluetoothSocket;
    private EXAsyncInputStreamReader inputStreamReader;
    private EXAsyncOutputStreamWriter outputStreamWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        messageClient = new WearableMessageClient(this, new WearableMessageClient.OnConnectionListener() {
            @Override
            public void onConnectedNode(Node node) {
                Log.d(FuturePlay.TAG, "wearable connected");
            }

            @Override
            public void onDisconnected(ConnectionResult result, int cause) {
                Log.d(FuturePlay.TAG, "wearable disconnected : "+result+" / "+cause);
            }
        });

        bandTouchSurfaceView = (BandTouchSurfaceView) findViewById(R.id.visualizationView);
        bandTouchRenderer = bandTouchSurfaceView.getRenderer();

        setOnSelectListener(this);
        resetResolution();

//        serialCommunicationProcessor = new SerialCommunicationProcessor(this, this);
//        serialCommunicationProcessor.start();
    }

    @Override
    protected void onStart() {
        super.onStart();

        resetResolution();
    }

    @Override
    protected void onResume() {
        super.onResume();

        bandTouchSurfaceView.onResume();

//        bandTouchRenderer.setListener(this);
//        generateData();
    }

    @Override
    protected void onPause() {
        super.onPause();

        bandTouchSurfaceView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        messageClient.disconnect();

        if (outputStreamWriter != null) {
            outputStreamWriter.stop();
        }
        if (inputStreamReader != null) {
            inputStreamReader.stop();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_connect) {
            searchDevices();
            return true;
        } else if (id == R.id.action_configuration) {
            Intent intent = new Intent(this, ConfigurationActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void resetResolution() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String syncConnPref = sharedPref.getString(ConfigurationActivity.KEY_RESOLUTION, getString(R.string.preference_resolution_default_value));
        String[] sizes = syncConnPref.split("x");

        if (sizes.length == 2) {
            int width = Integer.parseInt(sizes[0]);
            int height = Integer.parseInt(sizes[1]);

            bandTouchRenderer.setSize(width, height);
        }
    }

    private byte[] decode(byte[] src, int length) {
        int i=0; int j=0; int k=0;
        int value;
        byte[] decodeBuffer = new byte[expectRawDataSize() * 2];

        for(i = 0; (i < length && j < decodeBuffer.length); i++) {
            value = src[i] & 0xff;

            if(value == 0) {
                for(k = 0; (k < (src[i+1] & 0xff) && j < decodeBuffer.length); k++) {
                    decodeBuffer[j++] = (byte) 0;
                }
                i++;
            } else if(value == 255) {
                if ((src[i+1] & 0xff) == 255) {
                    break;
                }
            } else {
                decodeBuffer[j++] = src[i];
            }
        }

        return Arrays.copyOf(decodeBuffer, j);
    }

    private void requestData() {
        try {
            bluetoothSocket.getOutputStream().write(new byte[]{'F'});
            bluetoothSocket.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateData() {
        final byte[] data = new byte[expectRawDataSize() + 2];

        for (int i=0; i<data.length-2; i++) {
            data[i] = (byte) (Math.random() * 255);
        }

        data[data.length-2] = (byte) 0xff;
        data[data.length-1] = (byte) 0xff;

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onDataReceived(data, data.length);
                    }
                });
            }
        }).start();
    }

    private int expectRawDataSize() {
        return bandTouchRenderer.getGridWidth() * bandTouchRenderer.getGridHeight();
    }

    @Override
    public void onTouch(BandTouchEvent event) {
    }

    @Override
    public void onReceiveTouchPoint(BandTouchEvent topEvent, BandTouchEvent bottomEvent) {
    }

    @Override
    public void onReceiveRawTouchPoint(List<Integer> data) {
        bandTouchSurfaceView.inputRawTouchPoint(data);
    }

    @Override
    public void onSelect(BluetoothDevice bluetoothDevice) {
        try {
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(SERIAL_UUID);
            bluetoothSocket.connect();

            inputStreamReader = new EXAsyncInputStreamReader(bluetoothSocket.getInputStream(), this);
            outputStreamWriter = new EXAsyncOutputStreamWriter(bluetoothSocket.getOutputStream(), this);

            bandTouchRenderer.setListener(this);

            requestData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataReceived(byte[] buffer, int length) {
        byte[] decodedData = decode(buffer, length);

//        Log.d("FP", "-----------------------------------------");
//        Log.d("FP", "expected size : "+ expectRawDataSize());
//        Log.d("FP", "decode data size : "+decodedData.length);
//        Log.d("FP", EXDisplayUtil.getByteHexString(buffer));
//        Log.d("FP", EXDisplayUtil.getByteHexString(Arrays.copyOf(buffer, dataSize+2)));
//        Log.d("FP", EXDisplayUtil.getByteHexString(decodedData));

        bandTouchRenderer.setRawData(decodedData);
        bandTouchRenderer.requestDataDraw = true;
        
        bandTouchSurfaceView.requestRender();
    }

    @Override
    public void onException(Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onWriteException(IOException e) {
        e.printStackTrace();
    }

    @Override
    public void onClosed() {
        Log.d("FP", "closed");
    }

    @Override
    public void onWrite(int length) {
        Log.d("FP", "write success : "+length);
    }

    @Override
    public void onDrawSuccess() {
        if (bluetoothSocket != null && bluetoothSocket.isConnected()) {
            requestData();
        }
    }
}
