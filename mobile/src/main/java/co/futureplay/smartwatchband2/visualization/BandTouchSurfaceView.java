package co.futureplay.smartwatchband2.visualization;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

public class BandTouchSurfaceView extends VizSurfaceView {
    public BandTouchSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        BandTouchRenderer renderer = new BandTouchRenderer(3, 6);
        setRenderer(renderer);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public BandTouchRenderer getRenderer() {
        return (BandTouchRenderer) super.getRenderer();
    }

    public void inputRawTouchPoint(final List<Integer> data) {
        final int[] indexArray = {
                15,16,17,
                12,13,14,
                9,10,11,

                6,7,8,
                3,4,5,
                0,1,2,
        };

        final List<Integer> copiedData = new ArrayList<Integer>();

        for (Integer i : data) {
            copiedData.add(i);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (BandTouchRenderer.class) {
                    List<VizPoint> touchPointList = getRenderer().getTouchPointList();
                    touchPointList.clear();

                    if (copiedData.size() < 18) {
                        return;
                    }

                    for (int i = 0; i < 18; i++) {
                        int newIndex = indexArray[i];
                        int x = newIndex % 3;
                        int y = newIndex / 3;

                        VizPoint point = new VizPoint(x, y, copiedData.get(i));
                        touchPointList.add(point);
                    }
                }

                requestRender();
            }
        }).start();
    }
}
