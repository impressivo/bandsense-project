package co.futureplay.smartwatchband2.visualization;

public class VizColor {
    public float red;
    public float green;
    public float blue;
    public float alpha;

    private VizColor() {
    }

    public static VizColor color(float red, float green, float blue, float alpha) {
        VizColor color = new VizColor();
        color.red = red;
        color.green = green;
        color.blue = blue;
        color.alpha = alpha;
        return color;
    }

    public static VizColor color(float red, float green, float blue) {
        return VizColor.color(red, green, blue, 1.0f);
    }

    public static VizColor blackColor() {
        return VizColor.color(0.0f, 0, 0f, 0.0f);
    }

    public static VizColor whiteColor() {
        return VizColor.color(1.0f, 1.0f, 1.0f);
    }

    public static VizColor redColor() {
        return VizColor.color(1.0f, 0.0f, 0.0f);
    }

    public static VizColor greenColor() {
        return VizColor.color(0.0f, 1.0f, 0.0f);
    }

    public static VizColor blueColor() {
        return VizColor.color(0.0f, 0.0f, 1.0f);
    }
}
