package co.futureplay.smartwatchband2.visualization;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class BandTouchRenderer extends VizSurfaceView.Renderer {
    private float marginX;
    private float marginY;

    /** grid */
    public VizColor gridColor;
    private int gridWidth;
    private int gridHeight;
    private float gridInterval;

    public final List<VizPoint> touchPointList = new ArrayList<VizPoint>();

    private byte[] rawData;
    private Listener listener;
    public boolean requestDataDraw;

    public BandTouchRenderer(int gridWidth, int gridHeight) {
        setSize(gridWidth, gridHeight);
    }

    public void setSize(int gridWidth, int gridHeight) {
        float intervalX = 2.0f / (gridWidth - 1);
        float intervalY = 2.0f / (gridHeight- 1);

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.gridInterval = (intervalX < intervalY) ? intervalX : intervalY;
        this.marginX = (2.0f - (gridInterval * (gridWidth - 1))) / 2.0f;
        this.marginY = (2.0f - (gridInterval * (gridHeight - 1))) / 2.0f;
        this.rotateHorizontal = 45;
        this.gridColor = VizColor.whiteColor();
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    @Override
    public synchronized void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        drawCoordinates(gl, VizColor.redColor(), VizColor.greenColor(), VizColor.blueColor());
        drawGrid(gl);
//        int max = 0;
//
//        if (touchPointList.size() > 0) {
//            for (int i = 0; i < touchPointList.size(); i++) {
//                VizPoint point = touchPointList.get(i);
//
//                if (point != null && point.force > 0) {
//                    float x = -1.0f + marginX + (gridInterval * point.x);
//                    float y = -1.0f + marginY + (gridInterval * point.y);
//                    float height = point.force / 255.0f;
//                    float colors[] = {
//                            1.0f, 0.0f, 0.0f, 1.0f,
//                            0.9f, 0.0f, 0.0f, 1.0f,
//                            0.8f, 0.0f, 0.0f, 1.0f
//                    };
//
//                    drawHexahedron(gl, x, y, gridInterval, gridInterval, height, colors);
//                }
//            }
//        } else if (rawData != null && rawData.length > 0) {
        if (rawData == null) {
            return;
        }
            float rawX, rawY, rawForce;

            for (int i = 0; i < rawData.length; i++) {
                rawX = (getGridWidth() - 1) - (i % getGridWidth());
                rawY = i / getGridWidth();
                rawForce = (rawData[i] & 0xff);

                if (rawForce == 0) {
                    continue;
                }

//                max = (int) Math.max(max, rawForce);

                float x = -1.0f + marginX + (gridInterval * rawX);
                float y = -1.0f + marginY + (gridInterval * rawY);
                float height = rawForce / 255.0f;
                float colors[] = {
                        0.51f, 0.07f, 0.1f, 1.0f,
                        0.51f, 0.07f, 0.1f, 1.0f,
                        0.51f, 0.07f, 0.1f, 1.0f,
                };

                drawHexahedron(gl, x, y, gridInterval, gridInterval, height, colors);
            }

//            Log.d("FP", "max : "+max);
//        }

        if (requestDataDraw && listener != null) {
            requestDataDraw = false;
            listener.onDrawSuccess();
        }
    }

    private void drawGrid(GL10 gl) {
        for (float x = (-1.0f + marginX), i = 0; i < gridWidth; x += gridInterval, i++) {
            drawLine(gl, gridColor, new float[]{x, -1.0f + marginY, 0.0f, x, 1.0f - marginY, 0.0f});
        }

        for (float y = (-1.0f + marginY), i = 0; i < gridHeight; y += gridInterval, i++) {
            drawLine(gl, gridColor, new float[]{ -1.0f + marginX, y, 0.0f, 1.0f - marginX, y, 0.0f });
        }
    }

    public List<VizPoint> getTouchPointList() {
        return touchPointList;
    }

    public synchronized void setTouchPointList(List<VizPoint> pointList) {
        touchPointList.clear();
        touchPointList.addAll(pointList);
    }

    public void setRawData(byte[] data) {
        rawData = data;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        public void onDrawSuccess();
    }
}
