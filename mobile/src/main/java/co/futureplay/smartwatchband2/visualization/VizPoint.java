package co.futureplay.smartwatchband2.visualization;

public class VizPoint {
    public float x;
    public float y;
    public float force;

    public VizPoint(float x, float y, float force) {
        this.x = x;
        this.y = y;
        this.force = force;
    }

    public String toString() {
        return String.format("x:%.1f y:%.1f p:%.1f", x, y, force);
    }
}
