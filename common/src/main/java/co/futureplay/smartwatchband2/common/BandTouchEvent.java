package co.futureplay.smartwatchband2.common;

import android.os.SystemClock;

public class BandTouchEvent {
    public BandPosition position;
    public int index;
    public BandTouchEventAction action;
    public int x;
    public int y;
    public int force;
    public long time;

    public BandTouchEvent(BandPosition position, BandTouchEventAction action, int x, int y, int force) {
        this.position = position;
        this.action = action;
        this.x = x;
        this.y = y;
        this.force = force;
        this.time = SystemClock.uptimeMillis();
    }

    @Override
    public String toString() {
        return String.format("%s / %s / %d / (%d, %d) / %d", position, action, time, x, y, force);
    }

    @Override
    public boolean equals(Object o) {
        BandTouchEvent targetPoint = (BandTouchEvent) o;

        return x == targetPoint.x && y == targetPoint.y;
    }
}
