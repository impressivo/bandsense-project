package co.futureplay.smartwatchband2.common;

public class EXDisplayUtil {
    public static String getByteHexString(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        int count = 0;

        for (byte b : bytes) {
            result.append(String.format("%02x ", b));

            ++count;

            if (count == 10) {
                result.append("          ");
            } else if (count == 20) {
                result.append("\n");
                count = 0;
            }
        }

        return result.toString();
    }

    public static String getIntHexString(int[] bytes) {
        StringBuilder result = new StringBuilder();
        int count = 0;

        for (int b : bytes) {
            result.append(String.format("%02x ", b));

            ++count;

            if (count == 10) {
                result.append("          ");
            } else if (count == 20) {
                result.append("\n");
                count = 0;
            }
        }

        return result.toString();
    }

    public static String getByteHexInlineString(byte[] bytes) {
        StringBuilder result = new StringBuilder();

        for (byte b : bytes) {
            result.append(String.format("%02x ", b));
        }

        return result.toString();
    }
}
