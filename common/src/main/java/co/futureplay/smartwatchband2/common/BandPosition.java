package co.futureplay.smartwatchband2.common;

public enum BandPosition {
    TOP,
    BOTTOM
}
