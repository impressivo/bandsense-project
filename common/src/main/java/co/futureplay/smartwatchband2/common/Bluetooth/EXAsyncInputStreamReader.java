package co.futureplay.smartwatchband2.common.Bluetooth;

import java.io.InputStream;

public class EXAsyncInputStreamReader {
	private final InputStream inputStream;
	private Listener listener;
	private IoThread ioThread;

	public EXAsyncInputStreamReader(InputStream inputStream, Listener listener) {
		this.inputStream = inputStream;
		this.listener = listener;
		this.ioThread = new IoThread();
		
		ioThread.start();
	}
	
	public void stop() {
		if (ioThread != null) {
			ioThread.interrupt();
		}
	}

	private class IoThread extends Thread {
		@Override
		public void run() {
			byte[] buffer = new byte[2048];
            int index = 0;

			while (!Thread.interrupted()) {
				try {
                    buffer[index] = (byte) inputStream.read();

//                    Log.d("FP", "--------------------------");
//                    Log.d("FP", "available : "+inputStream.available());

                    if (index > 0 && buffer[index-1] == (byte)0xff && buffer[index] == (byte)0xff) {
                        if (listener != null) {
                            listener.onDataReceived(buffer, index+1);
                        }

//                        Log.d("FP", "available : "+inputStream.available());
                        inputStream.skip(inputStream.available());
                        index = 0;
                    } else {
                        index++;
                    }

//					int read = inputStream.read(buffer);
//					if (read > 0) {
//						if (listener != null) {
//							listener.onDataReceived(buffer, read);
//						}
//					}
				} catch (Exception e) {
					if (listener != null) {
						listener.onException(e);
					}
					break;
				}
			}
		}
	}

	public static interface Listener {
		void onDataReceived(byte[] buffer, int length);
		void onException(Exception e);
	}
}
