package co.futureplay.smartwatchband2.common;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class WearableMessageClient extends ContextObject {
    private GoogleApiClient apiClient;
    private Node node;
    private OnConnectionListener connectionListener;

    public WearableMessageClient(final Context context, final OnConnectionListener connListener) {
        super(context);

        this.connectionListener = connListener;

        apiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                List<Node> nodes = getNodes();

                                if (nodes.size() > 0) {
                                    node = nodes.get(0);
                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (connectionListener != null) {
                                            connectionListener.onConnectedNode(node);
                                        }
                                    }
                                });
                            }
                        }).start();
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        if (connectionListener != null) {
                            connectionListener.onDisconnected(null, cause);
                        }
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        if (connectionListener != null) {
                            connectionListener.onDisconnected(result, 0);
                        }
                    }
                })
                .addApi(Wearable.API)
                .build();
        apiClient.connect();
    }

    public void disconnect() {
        apiClient.disconnect();
    }

    public void addMessageListener(MessageApi.MessageListener listener) {
        Wearable.MessageApi.addListener(apiClient, listener);
    }

    public void removeMessageListener(MessageApi.MessageListener listener) {
        Wearable.MessageApi.removeListener(apiClient, listener);
    }

    private List<Node> getNodes() {
        NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(apiClient).await();
        List<Node> nodes = new ArrayList<Node>();

        for (int i=0; i<nodesResult.getNodes().size(); i++) {
            nodes.add(nodesResult.getNodes().get(i));
        }

        return nodes;
    }

    public void send(String path, int data) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.SIZE);
        buffer.putInt(data);
        sendMessage(path, buffer.array());
    }

    public void send(String path) {
        sendMessage(path, null);
    }

    public void sendMessage(String path, byte[] data) {
        if (apiClient == null || node == null) {
            return;
        }
        Wearable.MessageApi.sendMessage(apiClient, node.getId(), path, data);
    }

    public interface OnConnectionListener {
        public void onConnectedNode(Node node);
        public void onDisconnected(ConnectionResult result, int cause);
    }
}
