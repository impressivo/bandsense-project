package co.futureplay.smartwatchband2.common;

public enum BandGestureType {
    TAP,
    FLICK,
    PINCH_OUT,
    PINCH_IN,
    PINCH_OUT_END,
    PINCH_IN_END,
    ROTATE_CW,
    ROTATE_CCW,
    ROTATE_CW_END,
    ROTATE_CCW_END,
    MOVE,
    MOVE_END
}
