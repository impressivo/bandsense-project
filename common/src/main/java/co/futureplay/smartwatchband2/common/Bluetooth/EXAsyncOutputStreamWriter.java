package co.futureplay.smartwatchband2.common.Bluetooth;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class EXAsyncOutputStreamWriter {
	private OutputStream outputStream;
	private IoThread ioThread;
	private LinkedList<byte[]> queue = new LinkedList<byte[]>();
	private Listener listener;

	public EXAsyncOutputStreamWriter(final OutputStream outputStream, Listener listener) {
		this.outputStream = outputStream;
		this.listener = listener;

		ioThread = new IoThread();
		ioThread.start();
	}

	public void stop() {
		ioThread.interrupt();
	}

	public void write(byte[] buffer, int offset, int count) {
		byte[] dataToSend = new byte[count]; // Need to change for the performance issue. Maybe ring buffer
		System.arraycopy(buffer, offset, dataToSend, 0, count);

		synchronized (ioThread) {
			queue.addLast(dataToSend);
			ioThread.notifyAll();
		}
	}

	public class IoThread extends Thread {
		@Override
		public void run() {
			while (!Thread.interrupted()) {
				if (!queue.isEmpty()) {
					byte[] dataToSend = null;

                    try {
                        dataToSend = queue.removeFirst();
                    } catch (NoSuchElementException e) {
                        continue;
                    }

					try {
						outputStream.write(dataToSend);
						if (listener != null) {
							listener.onWrite(dataToSend.length);
						}
					} catch (IOException e) {
						if (listener != null) {
							listener.onWriteException(e);
						}
					}
				}

                if (queue.isEmpty()) {
                    synchronized (this) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
			}
			
			if (listener != null) {
				listener.onClosed();
			}
		}
	}
	
	public static interface Listener {
		void onWriteException(IOException e);
		void onClosed();
		void onWrite(int length);
	}
}
