package co.futureplay.smartwatchband2.common;

public class SingleTouchTracker {
    private final int moveXThreshold;
    private final int moveYThreshold;
    private State state = State.NOT_TOUCHED;
    private long downTime;
    private int downX;
    private int downY;
    private int x;
    private int y;

    public SingleTouchTracker(int moveXThreshold, int moveYThreshold) {
        this.moveXThreshold = moveXThreshold;
        this.moveYThreshold = moveYThreshold;
    }

    /**
     * 한 손가락의 TouchEvent 로부터 주요 상태 변화를 추적한다.
     * @param event 트래킹 중인 손가락의 터치 이벤트
     * @return 상태가 변경되면 true 를 반환한다.
     */
    public boolean onTouch(BandTouchEvent event) {
        x = event.x;
        y = event.y;

        if (state == State.NOT_TOUCHED) {
            if (event.action == BandTouchEventAction.DOWN) {
                state = State.TOUCHED_WAITING;
                downX = event.x;
                downY = event.y;
                downTime = event.time;
                return true;
            }
        } else if (state == State.TOUCHED_WAITING) {
            if (event.action == BandTouchEventAction.UP) {
                state = State.NOT_TOUCHED;
                return true;
            } else if (event.action == BandTouchEventAction.MOVE) {
                // TODO 많이 움직이면
                if (Math.abs(x - downX) >= moveXThreshold || Math.abs(y - downY) > moveYThreshold) {
                    state = State.TOUCHED_MOVING;
                    return true;
                }
            }
        } else if (state == State.TOUCHED_MOVING) {
            if (event.action == BandTouchEventAction.UP) {
                state = State.NOT_TOUCHED;
                return true;
            } else if (event.action == BandTouchEventAction.MOVE) {
                // TODO direction and velocity
            }
        }
        return false;
    }

    public State getState() {
        return state;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDownX() {
        return downX;
    }

    public int getDownY() {
        return downY;
    }

    public long getDownTime() {
        return downTime;
    }

    public static enum State {
        NOT_TOUCHED,
        TOUCHED_WAITING,
        TOUCHED_MOVING
    }
}
