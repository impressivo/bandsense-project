package co.futureplay.smartwatchband2.common;

public class BandGestureEvent {
    private BandGestureType type;
    private Integer data;
    private BandCoord.Band band;
    private BandCoord.PositionInBand positionInBand;

    public BandGestureEvent(BandGestureType type) {
        this.type = type;
    }

    public BandGestureEvent(BandGestureType type, Integer data) {
        this.type = type;
        this.data = data;
    }

    public BandGestureType getType() {
        return type;
    }

    public Integer getData() {
        return data;
    }

    public void setData(Integer data) {
        this.data = data;
    }

    public BandCoord.Band getBand() {
        return band;
    }

    public void setBand(BandCoord.Band band) {
        this.band = band;
    }

    public BandCoord.PositionInBand getPositionInBand() {
        return positionInBand;
    }

    public void setPositionInBand(BandCoord.PositionInBand positionInBand) {
        this.positionInBand = positionInBand;
    }
}
