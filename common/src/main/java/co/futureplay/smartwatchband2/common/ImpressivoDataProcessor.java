package co.futureplay.smartwatchband2.common;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 수신된 byte array 를 해석해서 BandTouchEvent 또는 RawBandTouchEvent 를 생성한다.
 */
public class ImpressivoDataProcessor {
    private final int MIN_PRESSURE = 50;
    private final Listener listener;
    private List<Integer> inputValues = new ArrayList<Integer>();
    private int endSymbolCount;
    private int topSum;
    private int topMax;
    private int bottomSum;
    private int bottomMax;
    private BandTouchEvent topLastTouchEvent;
    private BandTouchEvent bottomLastTouchEvent;

    public ImpressivoDataProcessor(Listener listener) {
        this.listener = listener;
    }

    public void onNewData(byte[] data) {
        onNewData(data, 0, data.length);
    }

    public void onNewData(byte[] data, int offset, int length) {
        for (int i = 0; i < length; i+= 8) {
            BandTouchEventAction action = BandTouchEventAction.fromByte(data[offset + i + 0]);

            if (action != null) {
                byte id = data[offset + i + 1];
                int x = readInt(data, offset + i + 2);
                int y = readInt(data, offset + i + 4);
                int z = readInt(data, offset + i + 6);

                BandTouchEvent event = new BandTouchEvent(null, action, x, y, z);
                event.index = id;
                listener.onTouch(event);
            }
        }

//        Log.d(FuturePlay.TAG, "raw : "+Arrays.toString(data));

//        for (int i = offset; i < length; i++) {
//            byte b = data[i];
//            int value = b & 0xff;
//
//            if (value == 0xff) {
//                endSymbolCount++;
//
//                if (endSymbolCount == 2) {
//                    dispatchEvent(topSum, topMax, bottomSum, bottomMax);
//
//                    listener.onReceiveRawTouchPoint(inputValues);
//
//                    inputValues.clear();
//                    endSymbolCount = 0;
//                    topSum = 0;
//                    topMax = 0;
//                    bottomSum = 0;
//                    bottomMax = 0;
//                }
//
//                continue;
//            }
//
//            value -= MIN_PRESSURE;
//            value = Math.max(0, value);
//
//            if (inputValues.size() < 9) {
//                topMax = Math.max(topMax, value);
//                topSum += value;
//            } else {
//                bottomMax = Math.max(bottomMax, value);
//                bottomSum += value;
//            }
//
//            endSymbolCount = 0;
//
//            inputValues.add(value);
//        }
    }

    private int readInt(byte[] data, int offset) {
        int i1 = 0xFF & data[offset];
        int i2 = 0xFF & data[offset + 1];
        return (i1 << 8) + i2;
    }

    private void setLastEvent(BandPosition position, BandTouchEvent event) {
        if (position == BandPosition.TOP) {
            topLastTouchEvent = event;
        }
        else {
            bottomLastTouchEvent = event;
        }
    }

    private void dispatchEvent(final int topSum, final int topMax, final int bottomSum, final int bottomMax) {
        Log.d(FuturePlay.TAG, "dispatch : " + String.valueOf(inputValues));
        BandTouchEvent topEvent = dispatchEvent(BandPosition.TOP, topLastTouchEvent, topSum, topMax);
        BandTouchEvent bottomEvent = dispatchEvent(BandPosition.BOTTOM, bottomLastTouchEvent, bottomSum, bottomMax);

        if (topEvent != null || bottomEvent != null) {
            listener.onReceiveTouchPoint(topEvent, bottomEvent);
        }
    }

    private BandTouchEvent dispatchEvent(BandPosition position, BandTouchEvent lastEvent, int sum, int max) {
        if (sum == 0 && lastEvent != null) {
            BandTouchEvent touchEvent = new BandTouchEvent(position, BandTouchEventAction.UP, lastEvent.x, lastEvent.y, 0);
            setLastEvent(position, null);

            return touchEvent;
        }
        else if (sum > 0){
            BandTouchEventAction action;

            if (lastEvent == null) {
                action = BandTouchEventAction.DOWN;
            }
            else {
                action = BandTouchEventAction.MOVE;
            }

            BandTouchEvent touchEvent = null;
            int start = (position == BandPosition.TOP) ? 0 : 9;

            for (int i = start; i < start+9; i++) {
                int value = inputValues.get(i);

                if (value == max) {
                    touchEvent = new BandTouchEvent(position, action, i / 3, i % 3, 0);
                    break;
                }
            }

            if (touchEvent != null) {
                if (lastEvent == null || !touchEvent.equals(lastEvent)) {
                    setLastEvent(position, touchEvent);

                    return touchEvent;
                }
            }
        }

        return null;
    }

    public interface Listener {
        public void onTouch(BandTouchEvent event);


        public void onReceiveTouchPoint(BandTouchEvent topEvent, BandTouchEvent bottomEvent);
        public void onReceiveRawTouchPoint(List<Integer> data);
    }
}
