package co.futureplay.smartwatchband2.common;

import android.content.Context;
import android.os.Handler;

public class ContextObject {
    private Context context;

    public ContextObject(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void runOnUiThread(Runnable runnable) {
        Handler mainHandler = new Handler(context.getMainLooper());
        mainHandler.post(runnable);
    }
}
