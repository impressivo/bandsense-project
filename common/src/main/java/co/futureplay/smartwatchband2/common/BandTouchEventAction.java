package co.futureplay.smartwatchband2.common;

public enum BandTouchEventAction {
    DOWN,
    MOVE,
    UP;

    public static BandTouchEventAction fromByte(byte b) {
        if (b == 0x00) {
            return DOWN;
        } else if (b == 0x04) {
            return UP;
        } else if (b == 0x07) {
            return MOVE;
        } else {
            return null;
        }
    }
}
