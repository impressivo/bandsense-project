package co.futureplay.smartwatchband2.common;

/**
 * Band 의 터치 좌표를 상대 좌표로 변환
 */
public class BandCoord {
    private final int x;
    private final int y;
    private final int width;
    private final int height;
    private final int verticalEdgeSize;
    private final int horizontalEdgeSize;
    private final Band band;
    private final PositionInBand positionInBand;
    private final int vx;
    private final int vy;

    public BandCoord(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.horizontalEdgeSize = Math.round(width / 3f);
        this.verticalEdgeSize = Math.round(height / 4f / 3f);

        if (y < height / 2) {
            band = Band.INSIDE;
            vx = x;
            vy = y;
        } else {
            band = Band.OUTSIDE;
            vx = x;
            vy = height - y - 1;
        }

        if (vy < verticalEdgeSize) {
            positionInBand = PositionInBand.TOP;
        } else if (vx < horizontalEdgeSize) {
            positionInBand = PositionInBand.LEFT;
        } else if (vx >= width - horizontalEdgeSize) {
            positionInBand = PositionInBand.RIGHT;
        } else if (vy >= verticalEdgeSize * 2) {
            positionInBand = PositionInBand.BOTTOM;
        } else {
            positionInBand = PositionInBand.CENTER;
        }
    }

    public Band getBand() {
        return band;
    }

    public PositionInBand getPosition() {
        return positionInBand;
    }

    public int getVx() {
        return vx;
    }

    public int getVy() {
        return vy;
    }

    /**
     * 이벤트가 몸 바깥쪽 밴드와, 몸쪽 밴드 중 어느쪽에서 발생했는지
     */
    public static enum Band {
        OUTSIDE, INSIDE
    }

    public static enum PositionInBand {
        TOP, LEFT, RIGHT, BOTTOM, CENTER
    }
}
