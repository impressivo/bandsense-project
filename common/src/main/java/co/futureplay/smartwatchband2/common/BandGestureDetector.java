package co.futureplay.smartwatchband2.common;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 터치 데이터로부터 gesture 인식
 */
public class BandGestureDetector {
    private static final int FLICK_TIME = 300;
    private int touchWidth = 6;
    private int touchHeight = 34;
    private Listener listener;
    private State state = State.NOT_TOUCHED;

    private int touchCount;
    private SingleTouchTracker[] touchTrackers = new SingleTouchTracker[5];
    private List<Integer> touchIndex = new ArrayList<Integer>();
    private BandGestureType currentTwoFingerGestureType;

    public BandGestureDetector(Listener listener) {
        this.listener = listener;
        for (int i = 0; i < touchTrackers.length; i++) {
            touchTrackers[i] = new SingleTouchTracker(2, 2);
        }
    }

    public void onTouchEvent(BandTouchEvent event) {
        if (event.index >= touchTrackers.length || event.index < 0 || event.index >= 2 || event.x >= touchWidth || event.y >= touchHeight) {
            return ;
        }

        Log.d("FP", touchCount+" : "+event.index+"/"+event.action+"/"+event.x+"/"+event.y);

        SingleTouchTracker touchTracker = touchTrackers[event.index];

        if (touchTracker.onTouch(event)) {
            if (event.action == BandTouchEventAction.DOWN) {
                touchCount++;
                touchIndex.add(event.index);
            } else if (event.action == BandTouchEventAction.UP) {
                if (touchCount > 0) {
                    touchCount--;
                }
                touchIndex.remove(Integer.valueOf(event.index));
            }

//            if (touchCount == 0) {
//                return;
//            }

            if (state == State.NOT_TOUCHED) {
                if (event.action == BandTouchEventAction.DOWN) {
                    state = State.SINGLE_TOUCH_DETECTING;
                }
            } else if (state == State.SINGLE_TOUCH_DETECTING) {
                if (touchCount == 0) {
                    assert touchTracker.getState() == SingleTouchTracker.State.NOT_TOUCHED;
                    assert event.action == BandTouchEventAction.UP;
                    state = State.NOT_TOUCHED;
                    processTap(touchTracker);
                } else if (touchCount == 1) {
                    assert touchTracker.getState() == SingleTouchTracker.State.TOUCHED_MOVING;
                    assert event.action == BandTouchEventAction.MOVE;

                    state = State.SINGLE_TOUCH_MOVING;
                    processMove(touchTracker);
                } else if (touchCount == 2) {
                    assert touchTracker.getState() == SingleTouchTracker.State.TOUCHED_WAITING;
                    assert event.action == BandTouchEventAction.DOWN;

                    state = State.DOUBLE_TOUCH_DETECTING;
                }
            } else if (state == State.SINGLE_TOUCH_MOVING) {
                if (touchCount == 0) {
                    assert touchTracker.getState() == SingleTouchTracker.State.NOT_TOUCHED;
                    assert event.action == BandTouchEventAction.UP;
                    state = State.NOT_TOUCHED;
                    processFlick(event, touchTracker);
                    fireGestureEvent(new BandGestureEvent(BandGestureType.MOVE_END));
                } else if (touchCount == 1) {
                    // never goes here
                } else {
                    assert touchTracker.getState() == SingleTouchTracker.State.TOUCHED_WAITING;
                    assert event.action == BandTouchEventAction.DOWN;

                    state = State.CANCELING;
                }
            } else if (state == State.DOUBLE_TOUCH_DETECTING) {
                if (touchCount == 2) {
                    assert touchTracker.getState() == SingleTouchTracker.State.TOUCHED_MOVING;
                    currentTwoFingerGestureType = detectTwoFingerGesture();
                    if (currentTwoFingerGestureType != null) {
                        state = State.DOUBLE_TOUCH_DETECTED;
                        fireGestureEvent(new BandGestureEvent(currentTwoFingerGestureType));
                    }
                } else {
                    state = State.CANCELING;
                }
            } else if (state == State.DOUBLE_TOUCH_DETECTED) {
                if (touchCount < 2) {
                    fireGestureEvent(new BandGestureEvent(getEndGestureType(currentTwoFingerGestureType)));
                    currentTwoFingerGestureType = null;
                    state = State.CANCELING;
                } else if (touchCount > 2) {
                    fireGestureEvent(new BandGestureEvent(getEndGestureType(currentTwoFingerGestureType)));
                    currentTwoFingerGestureType = null;
                    state = State.CANCELING;
                }
            } else if (state == State.CANCELING) {
                touchCount = 0;
                if (touchCount == 0) {
                    state = State.NOT_TOUCHED;
                } else {
                    // ignore all event
                }
            }

            System.out.println("state = " + state);
        }
    }

    private void processMove(SingleTouchTracker touchTracker) {
        BandCoord c1 = new BandCoord(touchTracker.getDownX(), touchTracker.getDownY(), touchWidth, touchHeight);
        BandCoord c2 = new BandCoord(touchTracker.getX(), touchTracker.getY(), touchWidth, touchHeight);

        BandGestureEvent gesture = new BandGestureEvent(BandGestureType.MOVE);
        gesture.setBand(c2.getBand());
        gesture.setPositionInBand(detectMoveDirection(c1, c2));

        if (gesture.getPositionInBand() != null) {
            fireGestureEvent(gesture);
        }
    }

    private void processTap(SingleTouchTracker touchTracker) {
        BandCoord bandCoord = new BandCoord(touchTracker.getX(), touchTracker.getY(), touchWidth, touchHeight);

        BandGestureEvent gesture = new BandGestureEvent(BandGestureType.TAP);
        gesture.setBand(bandCoord.getBand());
        gesture.setPositionInBand(bandCoord.getPosition());
        fireGestureEvent(gesture);
    }

    private void processFlick(BandTouchEvent event, SingleTouchTracker touchTracker) {
        if (event.time - touchTracker.getDownTime() < FLICK_TIME) {
            BandCoord startCoord = new BandCoord(touchTracker.getDownX(), touchTracker.getDownY(), touchWidth, touchHeight);
            BandCoord curCoord = new BandCoord(touchTracker.getX(), touchTracker.getY(), touchWidth, touchHeight);

            if (startCoord.getBand() == curCoord.getBand()) {
                BandCoord.PositionInBand direction = detectMoveDirection(startCoord, curCoord);
                if (direction != null) {
                    BandGestureEvent gesture = new BandGestureEvent(BandGestureType.FLICK);
                    gesture.setBand(curCoord.getBand());
                    gesture.setPositionInBand(direction);
                    fireGestureEvent(gesture);
                }
            }
        }
    }

    private BandGestureType detectTwoFingerGesture() {
        SingleTouchTracker tracker1 = null;
        SingleTouchTracker tracker2 = null;

        for (SingleTouchTracker tracker : touchTrackers) {
            if (tracker.getState() == SingleTouchTracker.State.TOUCHED_MOVING) {
                if (tracker1 == null) {
                    tracker1 = tracker;
                } else {
                    tracker2 = tracker;
                    break;
                }
            }
        }
        if (tracker2 == null) {
            return null;
        }

        BandCoord c11 = new BandCoord(tracker1.getDownX(), tracker1.getDownY(), touchWidth, touchHeight);
        BandCoord c12 = new BandCoord(tracker1.getX(), tracker1.getY(), touchWidth, touchHeight);
        BandCoord c21 = new BandCoord(tracker2.getDownX(), tracker2.getDownY(), touchWidth, touchHeight);
        BandCoord c22 = new BandCoord(tracker2.getX(), tracker2.getY(), touchWidth, touchHeight);

        // Filter out
        if (c11.getBand() != c12.getBand()) {
            return null;
        } else if (c21.getBand() != c22.getBand()) {
            return null;
        } else if (c11.getBand() == c21.getBand()) {
            return null;
        }

        BandCoord.PositionInBand dir1 = detectMoveDirection(c11, c12);
        BandCoord.PositionInBand dir2 = detectMoveDirection(c21, c22);

        if (dir1 == BandCoord.PositionInBand.BOTTOM && dir2 == BandCoord.PositionInBand.BOTTOM) {
            return BandGestureType.PINCH_OUT;
        } else if (dir1 == BandCoord.PositionInBand.TOP && dir2 == BandCoord.PositionInBand.TOP) {
            return BandGestureType.PINCH_IN;
        } else if (dir1 == BandCoord.PositionInBand.LEFT || dir2 == BandCoord.PositionInBand.RIGHT) {
            if (c11.getBand() == BandCoord.Band.INSIDE) {
                return BandGestureType.ROTATE_CW;
            } else {
                return BandGestureType.ROTATE_CCW;
            }
        } else if (dir1 == BandCoord.PositionInBand.RIGHT || dir2 == BandCoord.PositionInBand.LEFT) {
            if (c11.getBand() == BandCoord.Band.INSIDE) {
                return BandGestureType.ROTATE_CCW;
            } else {
                return BandGestureType.ROTATE_CW;
            }
        }
        return null;
    }

    private BandGestureType getEndGestureType(BandGestureType startType) {
        if (startType == BandGestureType.PINCH_IN) {
            return BandGestureType.PINCH_IN_END;
        } else if (startType == BandGestureType.PINCH_OUT) {
            return BandGestureType.PINCH_OUT_END;
        } else if (startType == BandGestureType.ROTATE_CCW) {
            return BandGestureType.ROTATE_CW_END;
        } else if (startType == BandGestureType.ROTATE_CW) {
            return BandGestureType.ROTATE_CCW_END;
        } else {
            return null;
        }
    }

    private BandCoord.PositionInBand detectMoveDirection(BandCoord startCoord, BandCoord endCoord) {
        if (startCoord.getBand() == endCoord.getBand()) {
            int dx = endCoord.getVx() - startCoord.getVx();
            int dy = endCoord.getVy() - startCoord.getVy();

            if (dx != 0 || dy != 0) {
                BandGestureEvent gesture = new BandGestureEvent(BandGestureType.FLICK);
                gesture.setBand(endCoord.getBand());

                if (Math.abs(dx) >= Math.abs(dy)) {
                    if (dx > 0) {
                        return BandCoord.PositionInBand.RIGHT;
                    } else {
                        return BandCoord.PositionInBand.LEFT;
                    }
                } else {
                    if (dy > 0) {
                        return BandCoord.PositionInBand.BOTTOM;
                    } else {
                        return BandCoord.PositionInBand.TOP;
                    }
                }
            }
        }
        return null;
    }

    private void fireGestureEvent(BandGestureEvent event) {
        System.out.println("GestureEvent " + event.getType() + " " + event.getBand() + " " + event.getPositionInBand());
        if (listener != null) {
            listener.onGesture(event);
        }
    }

    public interface Listener {
        public void onGesture(BandGestureEvent event);
    }

    private static enum State {
        NOT_TOUCHED,
        CANCELING,
        SINGLE_TOUCH_DETECTING,
        SINGLE_TOUCH_MOVING,
        DOUBLE_TOUCH_DETECTING,
        DOUBLE_TOUCH_DETECTED
    }
}
