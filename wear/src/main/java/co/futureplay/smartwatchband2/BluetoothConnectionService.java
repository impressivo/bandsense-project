package co.futureplay.smartwatchband2;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;

import co.futureplay.smartwatchband2.common.BandGestureDetector;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandTouchEvent;
import co.futureplay.smartwatchband2.common.EXDisplayUtil;
import co.futureplay.smartwatchband2.common.ImpressivoDataProcessor;

public class BluetoothConnectionService extends Service {
    private static final String TAG = BluetoothConnectionService.class.getSimpleName();
    private static final UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final long RETRY_INTERVAL = 5000;
    private static final String PREF_DEVICE_ADDRESS = "selected_device_address";
//    private static final byte[] REQUEST_BYTES = {'T'};

    private long lastReceivedTime = 0;
    private Timer timer;

    private SharedPreferences preferences;

    private List<StateListener> stateListeners = new ArrayList<StateListener>();
    private List<EventListener> eventListeners = new ArrayList<EventListener>();
    private ImpressivoDataProcessor impressivoDataProcessor;
    private BandGestureDetector bandGestureDetector;

    private BluetoothDevice selectedDevice;
    private State state = State.NOT_SELECTED;
    private Thread ioThread;
    private InputStream inputStream;
    private OutputStream outputStream;
    private BluetoothSocket socket;

    @Override
    public void onCreate() {
        super.onCreate();

        impressivoDataProcessor = new ImpressivoDataProcessor(dataListener);
        bandGestureDetector = new BandGestureDetector(touchListener);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        loadPreference();

        if (selectedDevice != null) {
            startIOThread();
        }

//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//                if (outputStream != null && (lastReceivedTime + 20) <System.currentTimeMillis()) {
//                    try {
//                        Log.d("FP", "retry");
//                        outputStream.write(REQUEST_BYTES);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        };
//
//        timer = new Timer();
//        timer.schedule(timerTask, 0, 10);
    }

    @Override
    public void onDestroy() {
        stopIOThread();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    private void loadPreference() {
        String address = preferences.getString(PREF_DEVICE_ADDRESS, null);
        if (address != null && !address.isEmpty()) {
            selectedDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
            state = State.CONNECTING;
        }
    }

    private void savePreference() {
        SharedPreferences.Editor editor = preferences.edit();
        if (selectedDevice != null) {
            editor.putString(PREF_DEVICE_ADDRESS, selectedDevice.getAddress());
        } else {
            editor.remove(PREF_DEVICE_ADDRESS);
        }
        editor.apply();
    }

    private void startIOThread() {
        ioThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.interrupted()) {
                    updateState(State.CONNECTING);
                    if (connect()) {
                        updateState(State.CONNECTED);
                        read();
                        updateState(State.DISCONNECTED);
                    } else {
                        updateState(State.CONNECTION_FAILED);
                    }

                    if (Thread.interrupted()) {
                        break;
                    } else {
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
                updateState(State.NOT_SELECTED);
            }
        });
        ioThread.start();
    }

    private void stopIOThread() {
        ioThread.interrupt();

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                Log.w(TAG, "Failed to close socket");
            }
            socket = null;
        }
    }

    private boolean connect() {
        try {
            socket = selectedDevice.createRfcommSocketToServiceRecord(SERIAL_UUID);
        } catch (IOException e) {
            Log.e(TAG, "Failed to create bluetooth socket", e);
            return false;
        }

        try {
            socket.connect();
        } catch (IOException e) {
            Log.e(TAG, "Failed to connect", e);

            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e1) {
                    Log.w(TAG, "Failed to close socket", e1);
                }
            }
            socket = null;
            return false;
        }

        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            lastReceivedTime = System.currentTimeMillis();
        } catch (IOException e) {
            Log.e(TAG, "Failed to get streams", e);
            try {
                socket.close();
            } catch (IOException e1) {
                Log.w(TAG, "Failed to close socket", e1);
            }
            socket = null;
            return false;
        }

        return true;
    }

    private void read() {
        byte[] buf = new byte[4000];
        boolean requested = false;

        while (true) {
            try {
                if (requested) {
                    lastReceivedTime = System.currentTimeMillis();
                    int len = inputStream.read(buf);
//                    if (len < 0) {
//                        break;
//                    }

//                    Log.d("FP", EXDisplayUtil.getByteHexString(Arrays.copyOf(buf,len)));
//                    Log.d(TAG, "Data received " + len);
                    impressivoDataProcessor.onNewData(buf, 0, len);
                    requested = false;
                } else {
                    Log.d(TAG, "Sending request bytes");
//                    outputStream.write(REQUEST_BYTES);
                    requested = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }

        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.w(TAG, "Failed to close inputStream");
            }
            inputStream = null;
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                Log.w(TAG, "Failed to close socket");
            }
            socket = null;
        }
    }

    private void updateState(State state) {
        this.state = state;
        for (StateListener listener : stateListeners) {
            listener.onStateChange(state);
        }
    }

    public static interface StateListener {
        void onStateChange(State state);
    }

    public static interface EventListener {
        void onBandGestureEvent(BandGestureEvent event);
    }

    public static enum State {
        NOT_SELECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTED,
        CONNECTION_FAILED
    }

    public class LocalBinder extends Binder {
        public BluetoothConnectionServiceClient getClient() {
            return new BluetoothConnectionServiceClient();
        }
    }

    public class BluetoothConnectionServiceClient {
        public BluetoothDevice getSelectedDevice() {
            return selectedDevice;
        }

        public State getStatus() {
            return state;
        }

        public void selectDevice(BluetoothDevice device) {
            selectedDevice = device;
            savePreference();
            startIOThread();
        }

        public void deselectDevice() {
            selectedDevice = null;
            savePreference();
            stopIOThread();
        }

        public void registerStateListener(StateListener stateListener) {
            stateListeners.add(stateListener);
        }

        public void unregisterStateListener(StateListener stateListener) {
            stateListeners.remove(stateListener);
        }

        public void registerEventListener(EventListener eventListener) {
            if (!eventListeners.contains(eventListener)) {
                eventListeners.add(eventListener);
            }
        }

        public void unregisterEventListener(EventListener eventListener) {
            eventListeners.remove(eventListener);
        }
    }

    private ImpressivoDataProcessor.Listener dataListener = new ImpressivoDataProcessor.Listener() {
        @Override
        public void onTouch(BandTouchEvent event) {
            bandGestureDetector.onTouchEvent(event);
        }

        @Override
        public void onReceiveTouchPoint(BandTouchEvent topEvent, BandTouchEvent bottomEvent) {
//            bandGestureDetector.onTouchEvent(topEvent, bottomEvent);
        }

        @Override
        public void onReceiveRawTouchPoint(List<Integer> data) {
        }
    };

    private BandGestureDetector.Listener touchListener = new BandGestureDetector.Listener() {
        @Override
        public void onGesture(BandGestureEvent event) {
            for (EventListener listener : eventListeners) {
                listener.onBandGestureEvent(event);
            }
        }
    };
}
