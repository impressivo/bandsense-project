package co.futureplay.smartwatchband2;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class BluetoothConnectionActivity extends Activity {
    private BluetoothConnectionService.BluetoothConnectionServiceClient serviceClient;
    private List<DeviceListItem> devices = new ArrayList<DeviceListItem>();
    private ArrayAdapter<DeviceListItem> deviceListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection_activity);

        for (BluetoothDevice device : BluetoothAdapter.getDefaultAdapter().getBondedDevices()) {
            devices.add(new DeviceListItem(device));
        }

        deviceListAdapter = new ArrayAdapter<DeviceListItem>(this, android.R.layout.simple_list_item_1, devices);

        ListView deviceListView = (ListView) findViewById(R.id.device_list);
        deviceListView.setAdapter(deviceListAdapter);
        deviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectDevice(devices.get(i).getDevice());
            }
        });

        findViewById(R.id.disconnect_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deselectDevice();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, BluetoothConnectionService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        unbindService(serviceConnection);
        super.onPause();
    }

    private void selectDevice(BluetoothDevice device) {
        if (serviceClient != null) {
            serviceClient.selectDevice(device);
            showConnectionStatus();
        }
    }

    private void deselectDevice() {
        if (serviceClient != null) {
            serviceClient.deselectDevice();
            showDeviceList();
        }
    }

    private void updateViews() {
        if (serviceClient.getSelectedDevice() == null) {
            showDeviceList();
        } else {
            showConnectionStatus();
        }
    }

    private void showDeviceList() {
        findViewById(R.id.device_list).setVisibility(View.VISIBLE);
        findViewById(R.id.connection_status).setVisibility(View.GONE);
    }

    private void showConnectionStatus() {
        findViewById(R.id.device_list).setVisibility(View.GONE);
        findViewById(R.id.connection_status).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.device_name)).setText(serviceClient.getSelectedDevice().getName());
        ((TextView) findViewById(R.id.status_text)).setText(serviceClient.getStatus().toString());
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            BluetoothConnectionService.LocalBinder binder = (BluetoothConnectionService.LocalBinder) iBinder;
            serviceClient = binder.getClient();
            serviceClient.registerStateListener(bluetoothConnectionStateListener);
            updateViews();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceClient.unregisterStateListener(bluetoothConnectionStateListener);
            serviceClient = null;
        }
    };

    private BluetoothConnectionService.StateListener bluetoothConnectionStateListener = new BluetoothConnectionService.StateListener() {
        @Override
        public void onStateChange(final BluetoothConnectionService.State state) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) findViewById(R.id.status_text)).setText(state.toString());
                }
            });
        }
    };

    private static class DeviceListItem {
        private final BluetoothDevice device;

        private DeviceListItem(BluetoothDevice device) {
            this.device = device;
        }

        public BluetoothDevice getDevice() {
            return device;
        }

        @Override
        public String toString() {
            return device.getName() + "\n" + device.getAddress();
        }
    }
}
