package co.futureplay.smartwatchband2;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.futureplay.smartwatchband2.common.BandCoord;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.FuturePlay;

public class Demo5Activity extends DemoActivity {
    private ViewPager viewPager;
    private DemoPagerAdapter adapter;
    private List<Integer> data = new ArrayList<Integer>();
    private boolean touched;
    private int copiedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo2_activity);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                viewPager = (ViewPager) stub.findViewById(R.id.viewPager);
                viewPager.setClipToPadding(false);

                adapter = new DemoPagerAdapter();
                viewPager.setAdapter(adapter);
            }
        });

        data.add(R.drawable.ic_launcher);
        data.add(R.drawable.ic_full_cancel);
    }

    @Override
    protected void onImpressivoEvent(BandGestureEvent event) {
        BandGestureType type = event.getType();
        Log.d(FuturePlay.TAG, "Demo5 Message received: " + type);

        if (!touched) {
            return;
        }

        if (type == BandGestureType.FLICK) {
            if (event.getPositionInBand() == BandCoord.PositionInBand.TOP) {
                if (copiedItem > 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            data.add(copiedItem);
                            adapter.notifyDataSetChanged();

                            Log.d(FuturePlay.TAG, "paste");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast toast = Toast.makeText(Demo5Activity.this, "paste", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP, 0, 0);
                                    toast.show();
                                }
                            });
                        }
                    });
                }
            } else if (event.getPositionInBand() == BandCoord.PositionInBand.BOTTOM) {
                copiedItem = data.get(viewPager.getCurrentItem());
                Log.d(FuturePlay.TAG, "copy");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(Demo5Activity.this, "copy", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP, 0, 0);
                        toast.show();
                    }
                });
            }
        }
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        touched = event.getAction() == MotionEvent.ACTION_DOWN;
//        Log.d(FuturePlay.TAG, "Demo5 touched : "+touched);
//        return true;
//    }

    public class DemoPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return data.size()+1;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        public int getItemPosition(Object object){
            return POSITION_NONE;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = View.inflate(Demo5Activity.this, R.layout.demo5_row, null);
            view.setTag(position);
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        touched = true;
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        touched = false;
                    }
                    Log.d(FuturePlay.TAG, "Demo5 touched : " + touched);
                    return false;
                }
            });

            if (position < data.size()) {
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
                imageView.setImageResource(data.get(position));
            }

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
