package co.futureplay.smartwatchband2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.futureplay.smartwatchband2.common.BandCoord;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.FuturePlay;

public class MainActivity extends DemoActivity {
    private WearableListView listView;
    private boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                listView = (WearableListView) stub.findViewById(R.id.listView);
                listView.setAdapter(new MainAdapter());
            }
        });

        startService(new Intent(this, BluetoothConnectionService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        running = true;
    }

    @Override
    protected void onPause() {
        running = false;
        super.onPause();
    }

    @Override
    protected void onImpressivoEvent(BandGestureEvent event) {
        if (!running) {
            return;
        }

        final BandGestureType type = event.getType();
        Log.d(FuturePlay.TAG, "Main Message received: " + type);

        if (type == BandGestureType.FLICK) {
            if (event.getPositionInBand() == BandCoord.PositionInBand.TOP) {
                listView.smoothScrollBy(0, 120);
                return;
            } else if (event.getPositionInBand() == BandCoord.PositionInBand.BOTTOM) {
                listView.smoothScrollBy(0, -120);
                return;
            }
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int x = listView.getWidth() / 2;
                final int y = listView.getHeight() / 2;

                final int action;

                if (type == BandGestureType.TAP) {
                    action = MotionEvent.ACTION_UP;
//                } else if (type == ImpressivoEvent.Type.TOUCH_DOWN)) {
//                    action = MotionEvent.ACTION_DOWN;
//                } else {
//                    action = MotionEvent.ACTION_CANCEL;
                    MotionEvent downEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, x, y, 0);
                    listView.dispatchTouchEvent(downEvent);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MotionEvent upEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis() + 100, MotionEvent.ACTION_UP, x, y, 0);
                                    ;
                                    listView.dispatchTouchEvent(upEvent);
                                }
                            });
                        }
                    }, 100);
                }

            }
        });


    }

    public class MainAdapter extends WearableListView.Adapter {
        private Class[] activityClasses = {
                Demo1Activity.class,
                Demo2Activity.class,
                Demo3Activity.class,
                Demo4Activity.class,
                Demo5Activity.class,
                BluetoothConnectionActivity.class
        };

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_row, viewGroup, false);
            return new WearableListView.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder viewHolder, final int i) {
            TextView textView = (TextView) viewHolder.itemView.findViewById(R.id.itemTextView);

            if (i < 5) {
                textView.setText(String.format("Demo %d", (i + 1)));
            } else {
                textView.setText("Setting");
            }

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, activityClasses[i]);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return activityClasses.length;
        }
    }
}
