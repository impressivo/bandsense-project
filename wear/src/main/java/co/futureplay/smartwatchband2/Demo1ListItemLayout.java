package co.futureplay.smartwatchband2;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

public class Demo1ListItemLayout extends FrameLayout implements WearableListView.Item {
    private final float mFadedTextAlpha;
    public static Demo1ListItemLayout currentItem;
    private float mScale;
    private TextView textView;
    private TextView deleteTextView;
    private TextView callTextView;

    public Demo1ListItemLayout(Context context) {
        this(context, null);
    }

    public Demo1ListItemLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Demo1ListItemLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mFadedTextAlpha = 40 / 100f;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        textView = (TextView) findViewById(R.id.contentTextView);
        deleteTextView = (TextView) findViewById(R.id.deleteTextView);
        callTextView = (TextView) findViewById(R.id.callTextView);
    }

    @Override
    public float getProximityMinValue() {
        return 1f;
    }

    @Override
    public float getProximityMaxValue() {
        return 1.6f;
    }

    @Override
    public float getCurrentProximityValue() {
        return mScale;
    }

    @Override
    public void setScalingAnimatorValue(float scale) {
        mScale = scale;
    }

    @Override
    public void onScaleUpStart() {
        textView.setAlpha(1f);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 48);
        currentItem = this;
    }

    @Override
    public void onScaleDownStart() {
        textView.setAlpha(mFadedTextAlpha);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        deleteTextView.setVisibility(INVISIBLE);
        callTextView.setVisibility(INVISIBLE);
        setPressed(false);
    }

    public void showDelete() {
        deleteTextView.setVisibility(VISIBLE);

        TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        animation.setDuration(500);
        animation.setInterpolator(new BounceInterpolator());
        deleteTextView.startAnimation(animation);

        if (callTextView.getVisibility() == VISIBLE) {
            TranslateAnimation outAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
            outAnimation.setDuration(500);
            outAnimation.setInterpolator(new BounceInterpolator());
            outAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    callTextView.setVisibility(INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            callTextView.startAnimation(outAnimation);
        }
    }

    public void showCall() {
        callTextView.setVisibility(VISIBLE);

        TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        animation.setDuration(500);
        animation.setInterpolator(new BounceInterpolator());
        callTextView.startAnimation(animation);

        if (deleteTextView.getVisibility() == VISIBLE) {
            TranslateAnimation outAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
            outAnimation.setDuration(500);
            outAnimation.setInterpolator(new BounceInterpolator());
            outAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    deleteTextView.setVisibility(INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            deleteTextView.startAnimation(outAnimation);
        }
    }
}
