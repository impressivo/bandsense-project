package co.futureplay.smartwatchband2;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ItemLayout extends FrameLayout implements WearableListView.Item {
    private final float mFadedTextAlpha;
    private float mScale;
    private TextView textView;

    public ItemLayout(Context context) {
        this(context, null);
    }

    public ItemLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mFadedTextAlpha = 40 / 100f;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        textView = (TextView) findViewById(R.id.itemTextView);
    }

    @Override
    public float getProximityMinValue() {
        return 1f;
    }

    @Override
    public float getProximityMaxValue() {
        return 1.6f;
    }

    @Override
    public float getCurrentProximityValue() {
        return mScale;
    }

    @Override
    public void setScalingAnimatorValue(float scale) {
        mScale = scale;
    }

    @Override
    public void onScaleUpStart() {
        textView.setAlpha(1f);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 48);
    }

    @Override
    public void onScaleDownStart() {
        textView.setAlpha(mFadedTextAlpha);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        setPressed(false);
    }
}
