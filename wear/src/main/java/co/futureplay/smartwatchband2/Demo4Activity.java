package co.futureplay.smartwatchband2;

import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import co.futureplay.smartwatchband2.common.BandCoord;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.FuturePlay;

public class Demo4Activity extends DemoActivity {
    private static final int INTERVAL = 100;
    private EditText editText;
    private LinearLayout callBackground;
    private BandGestureEvent currentEvent;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo4_activity);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                editText = (EditText) stub.findViewById(R.id.phoneNumber);
                callBackground = (LinearLayout) stub.findViewById(R.id.callBackground);

                setDialNumberClickEvent(stub, R.id.number1);
                setDialNumberClickEvent(stub, R.id.number2);
                setDialNumberClickEvent(stub, R.id.number3);
                setDialNumberClickEvent(stub, R.id.number4);
                setDialNumberClickEvent(stub, R.id.number5);
                setDialNumberClickEvent(stub, R.id.number6);
                setDialNumberClickEvent(stub, R.id.number7);
                setDialNumberClickEvent(stub, R.id.number8);
                setDialNumberClickEvent(stub, R.id.number9);
                setDialNumberClickEvent(stub, R.id.number0);
            }
        });
    }

    private void setDialNumberClickEvent(WatchViewStub stub, int resource) {
        final Button button = (Button) stub.findViewById(resource);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.getText().insert(editText.getSelectionStart(), button.getText());
            }
        });
    }

    @Override
    protected void onImpressivoEvent(final BandGestureEvent event) {
        final BandGestureType type = event.getType();
        final BandCoord.PositionInBand position = event.getPositionInBand();

        Log.d(FuturePlay.TAG, "Demo4 Message received: " + type);

        if (type == BandGestureType.FLICK) {
            if (position == BandCoord.PositionInBand.TOP) {
                call();
            }
        } else if (type == BandGestureType.TAP) {
            if (position == BandCoord.PositionInBand.TOP) {
                call();
            } else if (position == BandCoord.PositionInBand.BOTTOM) {
                delete();
            } else if (position == BandCoord.PositionInBand.LEFT) {
                move(-1);
            } else if (position == BandCoord.PositionInBand.RIGHT) {
                move(1);
            }
        } else if (type == BandGestureType.MOVE) {
            if (position != BandCoord.PositionInBand.TOP) {
                currentEvent = event;
                continueAction();
            }
        } else if (type == BandGestureType.MOVE_END) {
            currentEvent = null;
        }
    }

    private void continueAction() {
        if (currentEvent != null) {
            BandCoord.PositionInBand position = currentEvent.getPositionInBand();
            if (position == BandCoord.PositionInBand.BOTTOM) {
                delete();
            } else if (position == BandCoord.PositionInBand.LEFT) {
                move(-1);
            } else if (position == BandCoord.PositionInBand.RIGHT) {
                move(1);
            }

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    continueAction();
                }
            }, INTERVAL);
        }
    }

    private void move(int amount) {
        final int pos = editText.getSelectionStart() + amount;
        if (pos >= 0 && pos <= editText.getText().length()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    editText.setSelection(pos);
                }
            });
        }
    }

    private void call() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (callBackground.getAlpha() != 1.0f) {
                    callBackground.setAlpha(1.0f);
                } else {
                    callBackground.setAlpha(0.0f);
                }
            }
        });
    }

    private void delete() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int length = editText.getText().length();
                if (length <= 0 || editText.getSelectionStart() == 0) {
                    return;
                }
                editText.getText().delete(editText.getSelectionStart() - 1, editText.getSelectionStart());
            }
        });
    }
}
