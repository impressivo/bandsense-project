package co.futureplay.smartwatchband2;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.futureplay.smartwatchband2.common.BandCoord;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.FuturePlay;

public class Demo2Activity extends DemoActivity {
    private ViewPager viewPager;
    private DemoPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo2_activity);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                viewPager = (ViewPager) stub.findViewById(R.id.viewPager);
                viewPager.setClipToPadding(false);

                adapter = new DemoPagerAdapter();
                viewPager.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onImpressivoEvent(BandGestureEvent event) {
        Log.d(FuturePlay.TAG, "Demo2 Message received: " + event.getType());

        if (event.getType() == BandGestureType.FLICK) {
            if (event.getPositionInBand() == BandCoord.PositionInBand.RIGHT) {
                movePage(-1);
            } else if (event.getPositionInBand() == BandCoord.PositionInBand.LEFT) {
                movePage(1);
            }
        }
        else if (event.getType() == BandGestureType.TAP) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final View view = viewPager.getRootView().findViewWithTag(viewPager.getCurrentItem());
                    view.setPressed(true);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    view.setPressed(false);
                                }
                            });
                        }
                    }, 100);
                }
            });
        }
    }

    private void movePage(final int moveAmount) {
        final int pageIndex = Math.min(Math.max(viewPager.getCurrentItem() + moveAmount, 0), viewPager.getAdapter().getCount() - 1);

        if (pageIndex != viewPager.getCurrentItem()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem(pageIndex, true);
                }
            });
        }
    }

    public class DemoPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 300;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = View.inflate(Demo2Activity.this, R.layout.demo2_row, null);
            view.setTag(position);

            TextView textView = (TextView) view.findViewById(R.id.contentTextView);
            textView.setText(Integer.toString(position));

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
