package co.futureplay.smartwatchband2;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ScrollView;

import co.futureplay.smartwatchband2.common.BandCoord;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.FuturePlay;

public class Demo3Activity extends DemoActivity {
    private static final int ANIMATION_DURATION = 100;
    private static final int ROTATION_UNIT = 15;
    private static final int MOVE_UNIT = 50;
    private static final float SCALE_UNIT = 0.5f;
    private ImageView imageView;
    private float rotateAngle = 0.0f;
    private float zoomScale = 1.0f;
    private ScrollView verticalScrollView;
    private HorizontalScrollView horizontalScrollView;
    private Point displaySize;
    private BandGestureEvent currentEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo3_activity);

        displaySize = new Point();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(displaySize);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                imageView = (ImageView) stub.findViewById(R.id.imageView);

                verticalScrollView = (ScrollView) stub.findViewById(R.id.vScrollView);
                horizontalScrollView = (HorizontalScrollView) stub.findViewById(R.id.hScrollView);

                final Drawable drawable = imageView.getDrawable();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        verticalScrollView.setScrollY((drawable.getIntrinsicHeight() - displaySize.y) / 2);
                        horizontalScrollView.setScrollX((drawable.getIntrinsicWidth() - displaySize.x) / 2);
                    }
                }, 100);
            }
        });
    }

    private Animation animation(float startAngle, final float endAngle, float startScale, final float endScale) {
        final int pivotX = horizontalScrollView.getScrollX() + (displaySize.x / 2);
        final int pivotY = verticalScrollView.getScrollY() + (displaySize.y / 2);

        Log.d(FuturePlay.TAG, String.format("angle %f->%f / scale %f->%f", startAngle, endAngle, startScale, endScale));

        ScaleAnimation scaleAnimation = new ScaleAnimation(startScale, endScale, startScale, endScale, Animation.ABSOLUTE, pivotX, Animation.ABSOLUTE, pivotY);
        scaleAnimation.setDuration(100);
        scaleAnimation.setFillEnabled(true);
        scaleAnimation.setFillAfter(true);
//        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                zoomScale = scale;
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });

        RotateAnimation rotateAnimation = new RotateAnimation(startAngle, endAngle, RotateAnimation.ABSOLUTE, pivotX, RotateAnimation.ABSOLUTE, pivotY);
        rotateAnimation.setDuration(ANIMATION_DURATION);
        rotateAnimation.setFillEnabled(true);
        rotateAnimation.setFillAfter(true);
//        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                rotateAngle = angle;
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(ANIMATION_DURATION);
        animationSet.setFillEnabled(true);
        animationSet.setFillAfter(true);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);

        return animationSet;
    }

    @Override
    protected void onImpressivoEvent(final BandGestureEvent event) {
        Log.d(FuturePlay.TAG, "Demo3 Message received: " + event.getType());

        final BandGestureType type = event.getType();
        if (type == BandGestureType.PINCH_IN ||
                type == BandGestureType.PINCH_OUT ||
                type == BandGestureType.ROTATE_CW ||
                type == BandGestureType.ROTATE_CCW ||
                type == BandGestureType.MOVE ||
                type == BandGestureType.TAP ||
                type == BandGestureType.TAP) {
            currentEvent = event;
        } else if (type == BandGestureType.PINCH_IN_END ||
                type == BandGestureType.PINCH_OUT_END ||
                type == BandGestureType.ROTATE_CW_END ||
                type == BandGestureType.ROTATE_CCW_END ||
                type == BandGestureType.MOVE_END) {
            currentEvent = null;
        }

        if (currentEvent != null) {
            applyCurrentGesture();
        }
    }

    private void applyCurrentGesture() {
        final BandGestureType type = currentEvent.getType();
        final BandCoord.PositionInBand position = currentEvent.getPositionInBand();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (type == BandGestureType.PINCH_IN || type == BandGestureType.PINCH_OUT) {
                    float newScale;
                    if (type == BandGestureType.PINCH_IN) {
                        newScale = Math.max(1.0f, zoomScale - SCALE_UNIT);
                    } else {
                        newScale = Math.min(5.0f, zoomScale + SCALE_UNIT);
                    }
                    if (newScale != zoomScale) {
                        imageView.startAnimation(animation(rotateAngle, rotateAngle, zoomScale, newScale));
                        zoomScale = newScale;
                    }
                } else if (type == BandGestureType.ROTATE_CW || type == BandGestureType.ROTATE_CCW) {
                    float newAngle = rotateAngle;
                    if (type == BandGestureType.ROTATE_CW) {
                        newAngle += ROTATION_UNIT;
                    } else {
                        newAngle -= ROTATION_UNIT;
                    }
                    imageView.startAnimation(animation(rotateAngle, newAngle, zoomScale, zoomScale));
                    rotateAngle = newAngle;
                } else if (type == BandGestureType.MOVE || type == BandGestureType.TAP) {
                    if (position == BandCoord.PositionInBand.TOP) {
                        verticalScrollView.smoothScrollTo(verticalScrollView.getScrollX(), verticalScrollView.getScrollY() - MOVE_UNIT);
                    } else if (position == BandCoord.PositionInBand.BOTTOM) {
                        verticalScrollView.smoothScrollTo(verticalScrollView.getScrollX(), verticalScrollView.getScrollY() + MOVE_UNIT);
                    } else if (position == BandCoord.PositionInBand.LEFT) {
                        horizontalScrollView.smoothScrollTo(horizontalScrollView.getScrollX() - MOVE_UNIT, horizontalScrollView.getScrollY());
                    } else if (position == BandCoord.PositionInBand.RIGHT) {
                        horizontalScrollView.smoothScrollTo(horizontalScrollView.getScrollX() + MOVE_UNIT, horizontalScrollView.getScrollY());
                    }
                }
            }
        });

        if (type != BandGestureType.TAP) {
            imageView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (currentEvent != null) {
                        applyCurrentGesture();
                    }
                }
            }, ANIMATION_DURATION);
        }
    }
}
