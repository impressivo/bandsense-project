package co.futureplay.smartwatchband2;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.WindowManager;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;

import java.nio.ByteBuffer;

import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.WearableMessageClient;

public abstract class DemoActivity extends Activity {
    private BluetoothConnectionService.BluetoothConnectionServiceClient serviceClient;
    private WearableMessageClient messageClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        messageClient = new WearableMessageClient(this, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, BluetoothConnectionService.class), serviceConnection, BIND_AUTO_CREATE);
        messageClient.addMessageListener(messageListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (serviceClient != null) {
            serviceClient.unregisterEventListener(bandGestureEventListener);
        }
        unbindService(serviceConnection);
        messageClient.removeMessageListener(messageListener);
    }

    protected abstract void onImpressivoEvent(BandGestureEvent event);

    private MessageApi.MessageListener messageListener = new MessageApi.MessageListener() {
        @Override
        public void onMessageReceived(MessageEvent messageEvent) {
            String path = messageEvent.getPath();
            for (BandGestureType type : BandGestureType.values()) {
                if (type.toString().contentEquals(path)) {
                    BandGestureEvent event = new BandGestureEvent(type);
                    byte[] data = messageEvent.getData();
                    if (data != null && data.length >= Integer.SIZE / 8) {
                        ByteBuffer buf = ByteBuffer.wrap(data);
                        event.setData(buf.getInt());
                    }
                    onImpressivoEvent(event);
                    break;
                }
            }
        }
    };

    private BluetoothConnectionService.EventListener bandGestureEventListener = new BluetoothConnectionService.EventListener() {
        @Override
        public void onBandGestureEvent(BandGestureEvent event) {
            onImpressivoEvent(event);
        }
    };

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            BluetoothConnectionService.LocalBinder binder = (BluetoothConnectionService.LocalBinder) iBinder;
            serviceClient = binder.getClient();
            serviceClient.registerEventListener(bandGestureEventListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceClient.unregisterEventListener(bandGestureEventListener);
            serviceClient = null;
        }
    };
}
