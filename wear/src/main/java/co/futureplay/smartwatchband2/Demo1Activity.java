package co.futureplay.smartwatchband2;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.futureplay.smartwatchband2.common.BandCoord;
import co.futureplay.smartwatchband2.common.BandGestureEvent;
import co.futureplay.smartwatchband2.common.BandGestureType;
import co.futureplay.smartwatchband2.common.FuturePlay;

public class Demo1Activity extends DemoActivity {
    private WearableListView listView;
    private DemoAdapter adapter;

    private long downTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo1_activity);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                adapter = new DemoAdapter();

                listView = (WearableListView) stub.findViewById(R.id.listView);
                listView.setAdapter(adapter);
                listView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                    }
                });
            }
        });
    }

    @Override
    protected void onImpressivoEvent(BandGestureEvent event) {
        Log.d(FuturePlay.TAG, "Demo1 Message received: " + event.getType());

        if (event.getType() == BandGestureType.FLICK) {
            if (event.getPositionInBand() == BandCoord.PositionInBand.TOP) {
                listView.smoothScrollBy(0, 100);
            } else if (event.getPositionInBand() == BandCoord.PositionInBand.BOTTOM) {
                listView.smoothScrollBy(0, -100);
            } else if (event.getPositionInBand() == BandCoord.PositionInBand.LEFT) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Demo1ListItemLayout.currentItem.showDelete();
                    }
                });
            } else if (event.getPositionInBand() == BandCoord.PositionInBand.RIGHT) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Demo1ListItemLayout.currentItem.showCall();
                    }
                });
            }
//        } else if (path.contentEquals(BandPath.TOUCH_DOWN)) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    int x = listView.getWidth() / 2;
//                    int y = listView.getHeight() / 2;
//                    downTime = SystemClock.uptimeMillis();
//                    MotionEvent event = MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, x, y, 0);
//                    listView.dispatchTouchEvent(event);
//                }
//            });
//        } else if (path.contentEquals(BandPath.TOUCH_UP)) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    int x = listView.getWidth() / 2;
//                    int y = listView.getHeight() / 2;
//                    MotionEvent event = MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, x, y, 0);
//                    listView.dispatchTouchEvent(event);
//                }
//            });
        } else if (event.getType() == BandGestureType.TAP) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int x = listView.getWidth() / 2;
                    final int y = listView.getHeight() / 2;
                    MotionEvent downEvent = MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, x, y, 0);
                    listView.dispatchTouchEvent(downEvent);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MotionEvent upEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis() + 100, MotionEvent.ACTION_UP, x, y, 0);
                                    ;
                                    listView.dispatchTouchEvent(upEvent);
                                }
                            });
                        }
                    }, 100);
                }
            });
        }
    }

    public class DemoAdapter extends WearableListView.Adapter {

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.demo1_row, viewGroup, false);

            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            layoutParams.height = 100;

            return new WearableListView.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder viewHolder, int i) {
            TextView textView = (TextView) viewHolder.itemView.findViewById(R.id.contentTextView);
            textView.setText(getItem(i));
        }

        @Override
        public int getItemCount() {
            return 300;
        }

        public String getItem(int position) {
            return String.format("%d", position+1);
        }
    }
}
